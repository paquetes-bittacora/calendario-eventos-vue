"use strict";

const VueLoaderPlugin = require('vue-loader/lib/plugin');
const path = require('path');

module.exports = {
    mode: 'development',
    entry: {
        calendar:    './calendar.js'
    },
    resolve: {
        modules: ['node_modules'],
        alias: {
            vue: 'vue/dist/vue.js'
        },
    },
    output: {
        filename: '[name].js', // Nombre del archivo compilado, y abajo, la ruta donde se compilará
        path: path.resolve(__dirname, './dist/js/'),
    },
    module: {
        rules: [
            {
                test: /\.vue$/,
                loader: 'vue-loader',
            },
            {
                test: /\.vue$/,
                use: 'eslint-loader',
            },
            {
                test: /\.css$/,
                use: [
                    'vue-style-loader',
                    {loader: 'css-loader', options: {esModule: false}}
                ]
            },
        ]
    },
    plugins: [
        new VueLoaderPlugin()
    ]
};
