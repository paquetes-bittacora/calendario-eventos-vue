Calendario de eventos
---------------------
Módulo para mostrar un calendario de eventos. Depende de bittacora/calendario-eventos-api, que es el módulo encargado de procesar las llamadas AJAX que obtienen los eventos.

Instalación
-----

Debe incluirse el archivo dist/js/calendar.js en el HTML de la página en la que se va a mostrar el calendario.

Debe existir un div con el id "calendario-eventos" para que se muestre el componente de Vue.

En JS , debe existir la variable "eventsCalendarApiUrl" con la ruta a la API del módulo bittacora/calendario-eventos-api. Si no existe, se usará una por defecto (la utilizada en el proyecto del Ayuntamiento).
