"use strict";

import axios from "axios";

class EventsCalendarApi {
    getEvents(startDate, endDate, categoryId = null) {
        return axios.get(window.eventsCalendarApiUrl + 'getEvents/?startDate=' + startDate.date + '&endDate=' + endDate.date + '&category=' + categoryId)
            .then(function (response) {
                return response.data;
            });
    }

    getCategories(startDate, endDate) {
        return axios.get(window.eventsCalendarApiUrl + 'getCategories')
            .then(function (response) {
                return response.data;
            });
    }
}

export let eventsCalendarApi = new EventsCalendarApi();
