"use strict";

import Vue from 'vue';
import axios from 'axios';
import VueAxios from 'vue-axios';
import Calendar from "./components/Calendar.vue";
import CalendarCategories from "./components/CalendarCategories.vue";

Vue.use(VueAxios, axios);
Vue.prototype.$axios = axios;

if (typeof window.eventsCalendarApiUrl == 'undefined') {
    window.eventsCalendarApiUrl = '/calendario-eventos/';
}

if (document.getElementById('calendario-eventos')) {

    new Vue({
        el: '#calendario-eventos',
        render: h => h(Calendar, {
            props: {
                format:  document.getElementById('calendario-eventos').getAttribute('format')
            }
        })
    });
}

if (document.getElementById('calendario-categorias')) {

    new Vue({
        el: '#calendario-categorias',
        render: h => h(CalendarCategories)
    });
}
